import React from "react";
import { Classes } from "./Classes";
import { Assessment } from "./Assessment";
import { Tasks } from "./Tasks";
import { Navbar } from "./Navbar";
import { Welcome } from "./Welcome";
import { BrowserRouter, Route } from "react-router-dom";
import { SignUp } from "./SignUp"; 
import { LogIn } from "./LogIn"; 
import { ClassDetail } from "./ClassDetail"; 
import { TaskDetail } from "./TaskDetail"; 

export default class App extends React.Component {
  render() {
    return (
        <BrowserRouter>
          <div>
            <Navbar />
            <Route exact path="/" component={Welcome} />
            <Route exact path="/classes" component={Classes} />
            <Route path="/classes/:id" component={ClassDetail}/>
            <Route exact path="/tasks" component={Tasks} />
            <Route path="/tasks/:id" component={TaskDetail} />
            <Route path="/assessment" component={Assessment} />
            <Route path="/signup" component={SignUp} /> 
            <Route path="/login" component={LogIn} /> 
          </div>
        </BrowserRouter>
    );
  }
}
