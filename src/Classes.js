import React from "react";
import { Table } from "reactstrap";
import * as firebase from "firebase";
import { withRouter } from 'react-router-dom';

export class Classes extends React.Component {
  constructor() {
    super();
    this.state = {
      data: null
    };
  }

  componentDidMount() {
    const rootRef = firebase.database().ref();
    const classRef = rootRef.child("classes");
    classRef.on("value", snap => {
      this.setState({
        data: snap.val()
      });
    });
  }
  

  render() {

    if (firebase.auth().currentUser !== null) {
      var userEmail = firebase.auth().currentUser.email;
    }

    const data = this.state.data;
    var classData = [];

    //To be able to map data later
    if (data) {
      for (var key in data) {
        var dataEmail = data[key].userEmail
        if (dataEmail === userEmail) classData.push(data[key]);
      }
    }

    return (
      <div className="container">
        <h1>Classes</h1>
        <form className="form-horizontal">
          <fieldset>
            <h2>Add Class</h2>
            <div id="errorMessage" role="alert" className="hide alert alert-warning"></div>

            <div className="form-group">
              <label className="col-lg-2 control-label">Name</label>
              <div className="col-lg-10">
                <input
                  type="text"
                  className="form-control"
                  id="inputName"
                  placeholder="Name"
                  required
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-2 control-label">Password</label>
              <div className="col-lg-10">
                <input
                  type="text"
                  className="form-control"
                  id="inputPassword"
                  placeholder="Password"
                  required
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-2 control-label">Time Limit in Minutes</label>
              <div className="col-lg-10">
                <input
                  type="number"
                  className="form-control"
                  id="inputTimeLimit"
                  placeholder="e.g. 45"
                  min="1"
                  step="1"
                  required
                />
              </div>
            </div>

            <div className="form-group">
              <label className="col-lg-2 control-label"></label>
              <div className="col-lg-10">
                <button
                  type="submit"
                  className="btn btn-primary"
                  onClick={saveClass}
                >
                  Submit
                </button>
              </div>
            </div>

          </fieldset>
        </form>

        <div>
          <h2>Your Classes</h2>
          <Table hover size="sm" responsive>
            <thead>
              <tr>
                <th>Name</th>
                <th>Password</th>
                <th>Time Limit in Minutes</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {classData &&
                classData.map(myClass => (
                  <tr>
                    <td>{myClass.name}</td>
                    <td>{myClass.passw}</td>
                    <td>{myClass.timeLimit}</td>
                    <td><button class="btn btn-primary btn-xs" onClick={() => this.props.history.push(`/classes/${myClass.id}`)} key={myClass.id}>Edit</button></td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

function saveClass() {

  const inputName = document.getElementById("inputName").value;
  const inputPassword = document.getElementById("inputPassword").value;
  const inputTimeLimit = document.getElementById("inputTimeLimit").value;

  // Check if all required data is present
  // If not, display an error message and don't add class
  const errorMessage = document.getElementById("errorMessage");
  if (!inputName || !inputPassword || !inputTimeLimit) {
    errorMessage.classList.remove("hide");
    errorMessage.innerText = "Fill in all of the fields!";
    return;
  }

  const rootRef = firebase.database().ref();
  const classRef = rootRef.child("classes");
  const tasksInClassRef = rootRef.child("tasksInClass");

  var myRef = classRef.push();
  var key = myRef.key;

  if (firebase.auth().currentUser !== null) {
    var userEmail = firebase.auth().currentUser.email;
  }

  errorMessage.classList.add("hide");

  classRef.child(key).set({
    id: key,
    name: inputName,
    passw: inputPassword,
    timeLimit: inputTimeLimit,
    userEmail: userEmail
  });
  
  tasksInClassRef.child(key).set({
    sample: 'sample'
  });
}

export default withRouter(Classes);
