import React from "react";
import * as firebase from "firebase";

export class LogIn extends React.Component {
  render() {
    return (
      <div className="container">
        <br />
        <div className="form-horizontal">
          <fieldset>
            <legend>Log in</legend>
            <div className="form-group">
              <label className="col-lg-2 control-label">Email</label>
              <div className="col-lg-10">
                <input
                  type="text"
                  className="form-control"
                  id="email"
                  placeholder="Email"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="col-lg-2 control-label">Password</label>
              <div className="col-lg-10">
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  placeholder="Password"
                />
                <br />
                <div className="form-group">
                  <div className="col-lg-10 col-lg-offset-10,5">
                    <button
                      type="submit"
                      className="btn btn-primary"
                      onClick={this.logIn}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
        <p id="errorMessage" style={{ color: "red" }} className="hide">
          Tekst
        </p>
      </div>
    );
  }

  logIn() {
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    const errorMessage = document.getElementById("errorMessage");

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(
        function(user) {
          window.location.replace("/");
        },
        function(error) {
          errorMessage.classList.remove("hide");
          errorMessage.innerText = error.message;
        }
      );
    errorMessage.classList.add("hide");
  }
}
