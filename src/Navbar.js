import React from "react";
import { signOut } from "./index.js";

export class Navbar extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-inverse">
        <div className="container">
          <div className="navbar-header">
            <a className="navbar-brand" href="/">
              Mobile Nature App
            </a>
          </div>
          <ul className="nav navbar-nav">
            <li>
              <a id="tasks" className="hide" href="/tasks">
                Tasks
              </a>
            </li>
            <li>
              <a id="classes" className="hide" href="/classes">
                Classes
              </a>
            </li>
            {/* <li>
              <a id="assessment" className="hide" href="/assessment">
                Assessment
              </a>
            </li> */}
          </ul>
          <ul className="nav navbar-nav navbar-right">
            {/* <li>
              <a href="/">en</a>
            </li>
            <li>
              <a href="/">et</a>
            </li> */}
            <li>
              <a id="signUpButton" href="/signup">
                <span className="glyphicon glyphicon-user" /> Sign up
              </a>
            </li>
            <li>
              <a id="logInButton" href="/login">
                <span className="glyphicon glyphicon-log-in" /> Login
              </a>
            </li>
            <li>
              <a id="logOutButton" className="hide" href="/login" onClick={signOut}>
                Log Out
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
