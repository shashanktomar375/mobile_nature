import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import * as firebase from "firebase";

var config = {
  apiKey: "AIzaSyDHBcXQBFDnKSuCWDSgOpFrfpaCcdp3Uwg",
  authDomain: "natureapp-3a642.firebaseapp.com",
  databaseURL: "https://natureapp-3a642.firebaseio.com",
  projectId: "natureapp-3a642",
  storageBucket: "natureapp-3a642.appspot.com",
  messagingSenderId: "624471166818"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(firebaseUser => {
  if (firebaseUser) {
    const logInButton = document.getElementById("logInButton");
    logInButton.classList.add("hide");
    const signUpButton = document.getElementById("signUpButton");
    signUpButton.classList.add("hide");
    const logOutButton = document.getElementById("logOutButton");
    logOutButton.classList.remove("hide");
    const classes = document.getElementById("classes");
    classes.classList.remove("hide");
    const tasks = document.getElementById("tasks");
    tasks.classList.remove("hide");
    const assessment = document.getElementById("assessment");
    assessment.classList.remove("hide");
  } 
});

export function signOut() {
  firebase
    .auth()
    .signOut()
    .then(
      function() {
      },
      function(error) {
      }
    );
}

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
